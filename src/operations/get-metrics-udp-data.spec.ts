import assert from "assert";
import test from "tape-promise/tape.js";
import { withContext } from "../testing/index.js";

test("get-metrics-udp-data", t => withContext(async context => {
    const now = new Date();

    await initializeMocks();

    const client = context.createApplicationClient({
        basic: { username: "MQ==", password: "dHJ1ZQ==" },
    });

    {
        const response = await client.getMetricsUdpData({
            parameters: {
                location: ["AQ=="],
                source: "123.134.145.156",
            },
        });
        assert(response.status === 200);

        const responseEntity = await response.entity();

        t.deepEqual(
            responseEntity,
            {
                "AQ==": {
                    rttAvg: 28,
                    rttSd: 0.1,
                },
            },
        );
    }

    async function initializeMocks() {
        context.servers.auth.registerValidateClientSecretOperation(async incomingRequest => {
            const incomingEntity = await incomingRequest.entity();
            const clientId = incomingRequest.parameters.client;
            const clientSecret = incomingEntity.value;

            return {
                status: 200,
                parameters: {},
                entity() {
                    return { valid: clientId === "MQ==" && clientSecret === "dHJ1ZQ==" };
                },
            };
        });

        await context.services.pgPool.query(`
INSERT INTO public.measurement_partition( year, week, attached )
VALUES( EXTRACT(ISOYEAR FROM current_timestamp), EXTRACT(WEEK FROM current_timestamp), true );
`);

        await context.services.pgPool.query(`
INSERT INTO public.location( id, created_utc )
VALUES( E'\\x01', '2012-12-01T12:00Z' );

INSERT INTO public.beacon(
    id, version,
    ipv4, ipv6,
    client, location,
    created_utc, last_seen_utc
)
VALUES(
    '5457ef04-4702-4166-a996-b798474ee185', 'v0.1.0',
    '127.0.255.250', '2001:db8::8a2e:370:7334',
    E'\\x31', E'\\x01',
    '2012-12-01T12:00Z', '2012-12-01T12:00Z'
);
`);

        await context.services.pgPool.query(`
INSERT INTO public.measurement (
    timestamp_utc,
    source,
    client
)
VALUES(
    $1,
    '123.134.145.156',
    E'\\x31'
);
`, [now.toISOString()]);

        await context.services.pgPool.query(`
INSERT INTO public.sample_udp_data (
    timestamp_utc,
    source, beacon,
    raw, rtt_ms, stddev
)
VALUES(
    $1,
    '123.134.145.156', '5457ef04-4702-4166-a996-b798474ee185',
    '{}', 28, 0.1
);
`, [now.toISOString()]);
    }

}));
