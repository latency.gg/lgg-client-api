import * as clientApi from "@latency.gg/lgg-client-oas";
import { week } from "msecs";
import * as application from "../application/index.js";
import { withPgTransaction } from "../utils/index.js";

export function createGetMetricsUdpDataOperation(
    context: application.Context,
): clientApi.GetMetricsUdpDataOperationHandler<application.ServerAuthorization> {
    return async incomingRequest => {
        const now = new Date();
        const since = new Date(now.valueOf() - 1 * week);

        const source = incomingRequest.parameters.source;
        const locations = incomingRequest.parameters.location;
        const locationBuffers = locations.map(location => Buffer.from(location, "base64"));

        const queryResult = await withPgTransaction(
            context,
            "select-metrics",
            async pgClient => {
                const result = await pgClient.query(`
select beacon.location, avg(sample_udp_data.rtt_ms) as avg, avg(sample_udp_data.stddev) as sd
from public.measurement
inner join public.sample_udp_data
on measurement.timestamp_utc = sample_udp_data.timestamp_utc
and measurement.source = sample_udp_data.source
inner join public.beacon
on sample_udp_data.beacon = beacon.id
where measurement.timestamp_utc > $1
and measurement.source = $2
and beacon.location = any($3)
group by 1
;
`,
                    [
                        since.toISOString(),
                        source,
                        locationBuffers,
                    ],
                );
                return result;
            },
        );

        const entity = queryResult.rows.reduce<Record<string, { rttAvg: number, rttSd: number }>>(
            (entity, row) => {
                const location = row.location.toString("base64");
                entity[location] = {
                    rttAvg: parseFloat(row.avg),
                    rttSd: parseFloat(row.sd),
                };
                return entity;
            },
            {},
        );

        return {
            status: 200,
            parameters: {},
            entity() { return entity; },
        };
    };
}
