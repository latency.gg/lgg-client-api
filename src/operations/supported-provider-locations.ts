import * as clientApi from "@latency.gg/lgg-client-oas";
import { minute } from "msecs";
import * as application from "../application/index.js";
import { withPgTransaction } from "../utils/index.js";

export function createGetSupportedProviderLocationsOperation(
    context: application.Context,
): clientApi.GetSupportedProviderLocationsOperationHandler<application.ServerAuthorization> {
    return async (incomingRequest, authorization) => {
        const { client } = authorization.basic;
        const { provider } = incomingRequest.parameters;

        const now = new Date();
        const lastSeenMinimum = new Date(
            now.valueOf() - 1 * minute,
        );

        const queryResult = await withPgTransaction(
            context,
            "select-provider-locations",
            async pgClient => {
                const result = await pgClient.query(`
select distinct l.value as location
from public.beacon
inner join public.location_label as l
on l.location = beacon.location
inner join public.location_label as p
on p.location = beacon.location
where l.name = 'location'
and p.name = 'provider'
and p.value = $1
and beacon.last_seen_utc > $2
;
`,
                    [
                        provider,
                        lastSeenMinimum.toISOString(),
                    ],
                );
                return result;
            },
        );

        const locations = queryResult.rows.map(({ location }) => location);

        return {
            status: 202,
            parameters: {},
            entity() {
                return {
                    name: provider,
                    locations,
                };
            },
        };
    };
}
