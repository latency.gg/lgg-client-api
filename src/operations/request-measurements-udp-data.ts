import * as clientApi from "@latency.gg/lgg-client-oas";
import assert from "assert";
import { hour, minute } from "msecs";
import * as net from "net";
import tweetnacl from "tweetnacl-ts";
import * as application from "../application/index.js";
import { withPgTransaction } from "../utils/index.js";

export function createRequestMeasurementUdpDataOperation(
    context: application.Context,
): clientApi.RequestMeasurementUdpDataOperationHandler<application.ServerAuthorization> {
    return async (incomingRequest, authorization) => {
        const now = new Date();
        const since = new Date(now.valueOf() - 12 * hour);
        const lastSeenMinimum = new Date(now.valueOf() - 1 * minute);

        const source = incomingRequest.parameters.source;
        const locations = incomingRequest.parameters.location;
        const locationBuffers = locations.map(location => Buffer.from(location, "base64"));

        const ipVersion = net.isIP(source);

        const queryResult = await withPgTransaction(
            context,
            "stale-targets-udp-data",
            async pgClient => {
                const result = await pgClient.query(`
select beacon.location, beacon.public_key, beacon.ipv4, beacon.ipv6
from public.beacon
where beacon.location = any($3)
and beacon.last_seen_utc > $4
and beacon.location not in (
    select beacon.location
    from public.measurement
    inner join public.sample_udp_data
    on measurement.timestamp_utc = sample_udp_data.timestamp_utc
    and measurement.source = sample_udp_data.source
    inner join public.beacon
    on sample_udp_data.beacon = beacon.id
    where measurement.timestamp_utc > $1
    and measurement.source = $2
    and beacon.location = any($3)
)
;
`,
                    [
                        since.toISOString(),
                        source,
                        locationBuffers,
                        lastSeenMinimum.toISOString(),
                    ],
                );
                return result;
            },
        );

        if (queryResult.rowCount === 0) {
            return {
                status: 304,
                parameters: {},
            };
        }

        queryResult.rows.sort(() => Math.random() > 0.5 ? 1 : -1);

        const locationSet = new Set<string>();
        const targets = queryResult.rows.
            map<[string, string | null, Buffer]>(
                row => [
                    row.location.toString("base64"),
                    ipVersion === 4 ? row.ipv4 : ipVersion === 6 ? row.ipv6 : null,
                    row.public_key,
                ] ,
            ).
            filter(([, host]) => host != null).
            filter(([location]) => {
                if (locationSet.has(location)) {
                    return false;
                }
                else {
                    locationSet.add(location);
                    return true;
                }
            }).
            map(([, host, publicKey]) => {
                assert(host);

                if (publicKey != null) {
                    const encoder = new TextEncoder();
                    const messageBuffer = encoder.encode(now.toISOString() + "/" + source);
                    const tokenBuffer = Buffer.from(
                        tweetnacl.sealedbox(messageBuffer, publicKey),
                    );
                    const token = tokenBuffer.toString("base64");

                    return {
                        host,
                        token,
                    };
                }
                else {
                    return {
                        host,
                        token: "",
                    };
                }

            });

        const clientIdBuffer = authorization.basic.client;

        await withPgTransaction(
            context,
            "insert measurement",
            async pgClient => {
                const result = await pgClient.query(`
insert into public.measurement (
    timestamp_utc, source, client
)
values (
    $1, $2, $3
)
;
`,
                    [
                        now.toISOString(),
                        source,
                        clientIdBuffer,
                    ],
                );
                return result;
            },
        );

        return {
            status: 200,
            parameters: {},
            entity() {
                return {
                    timestamp: now.valueOf(),
                    targets,
                };
            },
        };
    };
}
